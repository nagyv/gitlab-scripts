<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>GitLab Scripts</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>
<body>
<div class="container-fluid">

# GitLab Scripts

<p class="lead">
  GitLab Scripts is a userscript for <a href="https://www.tampermonkey.net/" target="_blank">Tampermonkey</a> to enhance GitLab.
</p>

## Install

### Pre-requisites

Install [Tampermonkey](https://www.tampermonkey.net/)

*   for [Firefox](https://addons.mozilla.org/firefox/addon/tampermonkey/)
*   for [Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)

### Install GitLab Scripts

Clicking on the following link will install the GitLab Scripts userscript into your browser. It will open a Tampermonkey screen to confirm the installatin.

<a class="btn btn-primary" href="gitlab.user.js">Click here to install GitLab Scripts</a>

## Features

[This youtube playlist](https://www.youtube.com/playlist?list=PLUMnVZtnFZjb5U4jE8DycPdzMmdNnZcxK) collects all the feature demos.

* Automatically mention previous authors when replying to a thread ([demo](https://youtu.be/q1KL8rCVwUs))
* Snooze todos, so they disappear from the todo list for the given hours, then re-appear automatically ([demo](https://youtu.be/FNOp6EN6pZc))
* Often used issue/epic quick commands can be applied with a click ([demo](https://youtu.be/1f_MR-VBB-g))
* An issue can be added to a recently visited epic with a click ([demo](https://youtu.be/kthZb_wwOj8))
* Add labels to filter by clicking on them in a merge request list ([demo](https://youtu.be/wKrLUxl_MPs))
* WIP: Increase / Decrease all the markdown headers in the active textarea
* WIP: In the GitLab handbook, convert `<time>` tags to local time based on the `datetime` attribute

## Source code

GitLab Scripts is an open source project. You are welcome to contribute or just to check out [its source code](https://gitlab.com/nagyv/gitlab-scripts/).

It is [licensed under the MIT license](https://gitlab.com/nagyv/gitlab-scripts/-/blob/main/LICENSE).

</div>

<noscript>![](https://queue.simpleanalyticscdn.com/noscript.gif)</noscript>
</body>
</html>
