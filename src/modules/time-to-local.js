/**
 * 
 */

// Add the module to index.js
// Add the module name here and into config.js
const MODULENAME = 'time-to-local'

const utils = require("../utils");
const log = utils.logger(MODULENAME);

const datetimeRe = /.*(\d{2}:\d{2}).*/

function convertToLocal(timeMatch) {
    log('timeMatch', timeMatch)
}

function setupModule() {
    const times = document.querySelectorAll('time[datetime]')
    times.forEach(t => {
        const attr = t.getAttribute('datetime')
        t.textContent = attr.replace(datetimeRe, convertToLocal)
    })
}

module.exports = {
    shouldActivate: () => {
        return utils.isModuleEnabled(MODULENAME) && utils.isOnPage(/.*\/(issues|epics)\/.*/gi);
    },
    initialize: setupModule,
};

