/**
 * Change labels to filter on MR pages instead of providing a list of MRs with the given label only
 */

const MODULENAME = 'label-filtering'

const utils = require("../utils");
const log = utils.logger(MODULENAME);

function addActionsToLabel(label, currentFilter) {
  const [path, tmp] = decodeURIComponent(label.href).split('?')
  const oldParams = new URLSearchParams(tmp)

  currentFilter.forEach((value, key) => oldParams.append(key, value))
  label.href = `${path}?${oldParams.toString()}`
}

function setupModule() {
  log("Starting module", MODULENAME)

  const currentFilter = new URLSearchParams(window.location.search)

  const labels = document.querySelectorAll('.gl-label.gl-label-sm a')
  labels.forEach(label => addActionsToLabel(label, currentFilter))
}

module.exports = {
  shouldActivate: () => {
    return utils.isModuleEnabled(MODULENAME) && utils.isOnPage(/.*\/(merge_requests|issues)/gi);
  },
  initialize: () => utils.waitForElement('ul.content-list.issuable-list', setupModule),
};



