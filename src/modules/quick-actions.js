/**
 * Various quick actions in issue, epic and MR comments
 * 
 * TODO: make the "simple" actions configurable on UI
 */

const utils = require("../utils");
const storage = require("../storage");
const saveEpics = require("./save-epics");

const log = utils.logger('quick-actions');

const defaultActions = [
    ['/assign me', 'Mine'],
    ['/label ~"GitLab Core" ~"release post::primary"', 'Primary release post'],
    ['/milestone %Backlog', 'Backlog'],
]

function getActions() {
    const actions = storage.get('quick-actions')
    return actions || defaultActions
}

function addAction(actionText) {
    const descriptionField = getDescriptionField()
    descriptionField.val(`${descriptionField.val()}\n${actionText}`)
    descriptionField.scrollTop(descriptionField[0].scrollHeight);
}

function getDescriptionField() {
    const parts = window.location.href.split('?')[0].split('/')
    if(parts[parts.length -1] === 'new') {
        return $('textarea#issue_description')
    } else {
        return $('textarea#note-body')
    }
}

function addSnippetOptions(select) {
    const actions = getActions();
    log('Setting up actions', actions);

    actions.forEach(([action, text]) => {
        const el = document.createElement("option");
        el.textContent = text;
        el.value = action;
        select.appendChild(el);
    })
}

function addEpicOptions(select) {
    const epics = saveEpics.getEpics()
    log('Known epics', epics);

    const group = document.createElement('optgroup')
    group.label = "Add to epic"

    epics.forEach(d => {
        const el = document.createElement("option");
        el.textContent = `${d.epicTitle} (${d.epicId})`;
        el.value = `/epic ${d.epicPath}`;
        group.appendChild(el);
    })

    select.appendChild(group)
}

async function refreshEpics(ev) {
    ev.preventDefault()

    await storage.initialize()
    const group = document.querySelector('#gitlab-scripts_quick-actions_select optgroup')
    group.remove()

    const select = document.getElementById('gitlab-scripts_quick-actions_select')
    addEpicOptions(select)
}

function handleActionSelected(ev) {
    ev.preventDefault()
    if(ev.target.value == "") return
    
    addAction(ev.target.value)
}

function setupQuicActions() {
    utils.waitForElement('#gitlab-scripts_quick-actions_select', () => {
        const actionsSelect = document.getElementById('gitlab-scripts_quick-actions_select')
        addSnippetOptions(actionsSelect);
        addEpicOptions(actionsSelect);

        actionsSelect.addEventListener('change', handleActionSelected)
        document.getElementById('gitlab-scripts_quick-actions_refresh').addEventListener('click', refreshEpics)
    })

    const descriptionField = getDescriptionField()
    descriptionField
    .closest('.md-write-holder')
    .append([
      `Quick actions: `,
      `<select id="gitlab-scripts_quick-actions_select">`,
      `<option value="" disabled selected>Apply action</option>`,
      `</select>`,
      `<button type="button" id="gitlab-scripts_quick-actions_refresh" data-action='refresh'><svg role="img" aria-hidden="true" class="link-highlight award-control-icon-positive gl-m-0! gl-icon s16" data-testid="retry-icon"><use href="/assets/icons-a4f9af4e415a4dd883fa4e3b4b89c5ce88dae1845135625d4c9b046124b36a1e.svg#retry"></use></svg></button>`,
    ].join(''))
}

module.exports = {
    shouldActivate: () => {
        return utils.isModuleEnabled("quick-actions") && utils.isOnPage(/.*\/(issues|epics)\/.*/gi);
    },
    initialize: setupQuicActions,
};


