/**
 * Snooze todos.
 * 
 * The idea and part of the implementation is from https://gitlab.com/marknuzzo/tanuki-snoozer
 */

const utils = require("../utils");
const storage = require("../storage");

const log = utils.logger('snooze-todos');

const defaultSnoozedList = []

function getSnoozed() {
    const data = storage.get("snoozed-todos")
    return JSON.parse(data) || defaultSnoozedList
}

function save_snooze(id, hours) {
    let snoozed = getSnoozed()
    const newItem = {
        id,
        hours,
        when: Date.now()
    }
    snoozed.push(newItem)
    return storage.set("snoozed-todos", JSON.stringify(snoozed))
}

function snooze(ev) {
    let elem = ev.target.parentElement.parentElement.parentElement;

    let hours = prompt("How many hours to snooze?", "24");
    if (hours == null || hours == "") {
        log("User cancelled the prompt.")
    } else {
        // Not a number so modify to default of 24 hours.
        if(isNaN(hours)){
            hours = 24;
        }
        // Go ahead and hide element/snooze item
        elem.remove();
        save_snooze(elem.id, parseFloat(hours, 10));
    }
}

function addButton() {
    const todos = document.getElementsByClassName('gl-flex-direction-row');
    for (let i = 0; i < todos.length; i++ ) {
        const doneWrapper = todos[i].querySelector('.todo-actions')

        const div = document.createElement('div');
        div.className = 'todo-actions gl-ml-3';

        const btn = document.createElement('a');
        btn.className = 'gl-button btn btn-default btn-loading d-flex align-items-center js-done-todo';
        btn.innerText = 'Snooze';
        btn.addEventListener('click', snooze, false);    

        div.append(btn);
        todos[i].insertBefore(div, doneWrapper)
    }
}

function toggleHidden(ev) {
    ev.preventDefault()
    ev.target.innerText = ev.target.dataset.status == 'hidden' ? 'Hide Snoozed' : 'Show Snoozed'
    ev.target.dataset.status = ev.target.dataset.status == 'hidden' ? 'shown' : 'hidden'

    const todos = document.querySelectorAll(".todo-pending.snoozed");
    todos.forEach(todo => todo.classList.toggle("hidden"))
}

function addGlobalToggleButton() {
    const DoneAllBtnWrap = document.querySelector('.top-area .nav-controls')

    const wrapDiv = document.createElement('div');
    wrapDiv.className = 'gl-mr-3'

    const btn = document.createElement('a');
    btn.className = 'gl-button btn btn-default btn-loading align-items-center';
    btn.innerText = 'Show Snoozed'
    btn.dataset.status = 'hidden'
    btn.addEventListener('click', toggleHidden, false);    

    wrapDiv.append(btn)
    DoneAllBtnWrap.prepend(wrapDiv)
}

async function hideSnoozed() {
    const todos = document.getElementsByClassName("todo-pending");
    const snoozed = getSnoozed()
    let toRemove = [];
    for (let i = 0; i < todos.length; i++) {
        const todoId = todos[i].id;
        for (let j=0; j < snoozed.length; j++) {
            const snoozedId = snoozed[j].id;
            const snoozedAt = snoozed[j].when;
            const hoursToSnooze = snoozed[j].hours;

            if (todoId === snoozedId) {
                // if when is between now and 24-hours ago
                // var hoursToSnooze = 24;
                if (snoozedAt > (Date.now() - (1000 * 60 * 60 * hoursToSnooze))) {
                    toRemove.push(todos[i]);
                    continue;
                } else {
                    // it's expired. clear from storage
                    
                    // to remove from storage, need to remove from the array
                    // and read the array under the same key
                }
            }
        }
    }
    for (let i = 0; i < toRemove.length; i++) {
        toRemove[i].classList.add('hidden', 'snoozed');
    }
}

function snoozeTodos() {
    addButton();
    hideSnoozed();
    addGlobalToggleButton();
}

module.exports = {
    shouldActivate: () => {
        return utils.isModuleEnabled("snooze-todos") && utils.isOnPage(/dashboard\/todos/gi);
    },
    initialize: snoozeTodos,
};

