/**
 * 
 */

// Add the module to index.js
// Add the module name here and into config.js
const MODULENAME = 'increase-headers'

const utils = require("../utils");
//const storage = require("../storage");
//const log = utils.logger(MODULENAME);

function setupModule() {
  log('Started')  
}

module.exports = {
    shouldActivate: () => {
        return utils.isModuleEnabled(MODULENAME) && utils.isOnPage(/.*\/(issues|epics|merge_requests)\/.*/gi);
    },
    initialize: setupModule,
};

