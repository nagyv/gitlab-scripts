/**
 * Mention the previous authors in a thread when replying
 */

const utils = require("../utils");
const storage = require("../storage");

const log = utils.logger('reply-last-author');

const responseObserverConfig = { attributes: true, attributeFilter: ['class'] };
const addUsernamesToResponseWhenActive = function(mutationsList, observer) {
    for(const mutation of mutationsList) {
        if (mutation.type === 'attributes' && mutation.target.classList.contains('is-replying')) {
            addUsernamesToResponse(mutation.target.querySelector('textarea'), mutation.target.dataset.usernames)
        }
    }
};
const responseObserver = new MutationObserver(addUsernamesToResponseWhenActive);

function handleTopLevelResponse(ev) {
    if(!ev.data || !ev.data.usernames) {
        log('No usernames passed. Exiting.')
        return
    }

    log('Running last author name handler', ev.data.usernames)
    addUsernamesToResponse('#note_note', ev.data.usernames)
}

function addUsernamesToResponse(textarea, usernames) {
    const $textarea = $(textarea);
    $textarea.val(`${usernames} `);
}

function getUsernamesFromComments(comments) {
    // Using Set makes the usernames unique by design
    const usernames = new Set();

    $(comments).find('.author-name-link.js-user-link').each((idx, el) => {
        usernames.add(`@${$(el).attr('data-username')}`);
    })
    log('Found username', [...usernames]);

    // Remove current user
    const current_user = storage.get('username');
    if(current_user) {
        usernames.delete(`@${current_user}`)
    }

    return [...usernames].join(' ');
}

function findUserNamesFromReplyBtn(btnEl) {
    const comments = btnEl.closest('li.note');

    return getUsernamesFromComments(comments);
}

function findUserNamesFromResponseArea(areaEl) {
    let comments = areaEl.closest('ul.notes').querySelectorAll('li.note')
    return getUsernamesFromComments(comments);
}

function findReplyButtons() {
    log('Checking init conditions for reply-last-author')

    if(!document.getElementById('notes-list') || !document.getElementById('notes-list').firstChild.id) {
        log('Delaying init for reply-last-author')
        window.setTimeout(findReplyButtons, 100);
        return
    }

    log('Initializing reply-last-author')

    // Comments without a comment need special treatment
    // Get all the .note-wrapper elements. These are the comments without a response. Find their reply button.
    document.querySelectorAll('#notes-list > .note-wrapper button.btn.js-reply-button').forEach((el) => {
        const usernames = findUserNamesFromReplyBtn(el);
        $(el).click({usernames}, handleTopLevelResponse);
    })

    document.querySelectorAll('.discussion-reply-holder').forEach((el) => {
        el.dataset.usernames = findUserNamesFromResponseArea(el);
        responseObserver.observe(el, responseObserverConfig);
    })
}

module.exports = {
    shouldActivate: () => {
        return utils.isModuleEnabled("reply-last-author") && utils.isOnPage(/.*\/(issues|epics|merge_requests)\/.*/gi);
    },
    initialize: findReplyButtons,
    //gl.startup_calls[`${window.location.pathname}/discussions.json`].fetchCall.then(findReplyButtons),
};
