/**
 * 
 */

// Add the module to index.js
// Add the module name here and into config.js
const MODULENAME = ''

const utils = require("../utils");
const storage = require("../storage");
const log = utils.logger(MODULENAME);

function setupModule() {
    
}

module.exports = {
    shouldActivate: () => {
        return utils.isModuleEnabled(MODULENAME) && utils.isOnPage(/.*\/(issues|epics)\/.*/gi);
    },
    initialize: setupModule,
};
