/**
 * Sets the current user for other modules
 */

const utils = require("../utils");
const storage = require('../storage');

const log = utils.logger('current-user');

function getUser() {
    const username = $('.current-user a').attr('data-user')
    if(username) {
        storage.set('username', username)
    }
    log(`Found user ${username}`)
}

module.exports = {
    shouldActivate: () => true,
    initialize: getUser,
}
