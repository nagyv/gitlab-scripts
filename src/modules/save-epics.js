/**
 * Save the visited epics for other modules, like quick-actions
 */

const utils = require("../utils");
const storage = require("../storage");
const config = require("../config");

const log = utils.logger('save-epics');

const storageKey = 'saved-epics'
function getEpics() {
    return storage.get(storageKey) || []
}

function saveEpic() {
    const data = {
        epicTitle: document.querySelector('h2.title').textContent,
        epicId: window.location.pathname.split('/').slice(-1)[0],
        epicPath: window.location.protocol + '//' + window.location.hostname + window.location.pathname,
    }
    
    // Save only 7 unique epics
    let savedEpics = getEpics()
    savedEpics = savedEpics.filter(prev => prev.epicId != data.epicId)
    const newLength = savedEpics.unshift(data)
    if(newLength > config.saveEpics.savedElements) {
        savedEpics.pop()
    }

    storage.set(storageKey, savedEpics)
}

module.exports = {
    shouldActivate: () => {
        return utils.isModuleEnabled("save-epics") && utils.isOnPage(/.*\/(epics)\/\d+/gi);
    },
    // Sometimes the title is not ready yet when the page already claims to be loaded
    initialize: () => utils.waitForElement('h2.title', saveEpic),
    getEpics,
};
