const config = require('./config')

function waitForElement(domQuery, next) {
  const observer = new MutationObserver((mutations, obs) => {
      const hello = document.querySelector(domQuery);
      if (hello) {
          obs.disconnect();
          next()
          return;
      }
  });

  observer.observe(document, {
      childList: true,
      subtree: true
  });
}

function waitForNoElement(domQuery, next) {
  const observer = new MutationObserver((mutations, obs) => {
      const hello = document.querySelector(domQuery);
      if (!hello) {
          obs.disconnect();
          next()
          return;
      }
  });

  observer.observe(document, {
      childList: true,
      subtree: true
  });
}

function isModuleEnabled(moduleName) {
  return !! config.modules[moduleName]
}

function isOnPage(path) {
    return window.location.pathname.match(path)
}

// Reads the value at the provided path in a deeply nested object
// From: https://raw.githubusercontent.com/solymosi/npu/master/src/utils.js
function deepGetProp(o, s) {
  let c = o;
  while (s.length) {
    const n = s.shift();
    if (!(c instanceof Object && n in c)) {
      return;
    }
    c = c[n];
  }
  return c;
}

// Sets a value at the provided path in a deeply nested object
// From: https://raw.githubusercontent.com/solymosi/npu/master/src/utils.js
function deepSetProp(o, s, v) {
  let c = o;
  while (s.length) {
    const n = s.shift();
    if (s.length === 0) {
      if (v === null) {
        delete c[n];
      } else {
        c[n] = v;
      }
      return;
    }
    if (!(typeof c === "object" && n in c)) {
      c[n] = new Object();
    }
    c = c[n];
  }
}

function logger(module) {
  if (!config.debug) {
    return () => {}
  }
  
  return function log(msg, data) {
    console.log(`[${module}] ${msg}`, data)
  }
}

module.exports = {
  isModuleEnabled,
  isOnPage,
  waitForElement,
  waitForNoElement,
  deepGetProp,
  deepSetProp,
  logger,
}
