module.exports = {
  debug: false,
  modules: {
      "reply-last-author": true,
      "snooze-todos": true,
      "quick-actions": true,
      "save-epics": true,
      'label-filtering': true,
      "increase-headers": true,
      "time-to-local": false,
  },
  saveEpics: {
    savedElements: 7
  }
}
