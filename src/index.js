const storage = require('./storage')

const modules = [
    require("./modules/current-user.js"),
    require("./modules/reply-last-author.js"),
    require("./modules/snooze-todos.js"),
    require("./modules/quick-actions.js"),
    require("./modules/save-epics.js"),
    require("./modules/label-filtering.js"),
    require("./modules/increase-headers.js"),
    require("./modules/time-to-local.js"),
]

async function init() {
  await storage.initialize();

  modules.forEach(module => {
    if (module.shouldActivate()) {
      module.initialize();
    }
  });

}

init();
