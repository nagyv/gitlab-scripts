// From https://github.com/solymosi/npu/blob/master/src/storage.js
const utils = require("./utils");

// Stored data
let data = {};

// Load all data from local storage
async function initialize() {
  try {
    data = JSON.parse(await GM.getValue("data")) || {};
  } catch (e) {
    data = {}
  }
  await upgradeSchema();
}

// Save all data to local storage
function save() {
  return GM.setValue("data", JSON.stringify(data));
}

// Gets the value at the specified key path
function get(...keys) {
  return utils.deepGetProp(data, keys);
}

// Sets the value at the specified key path
function set(...keysAndValue) {
  const value = keysAndValue.pop();
  const keys = keysAndValue;
  utils.deepSetProp(data, keys, value);
  save();
}

// Upgrade the data schema to the latest version
async function upgradeSchema() {
  const ver = typeof data.version !== "undefined" ? data.version : 0;

  // nothing to do

  save();
}

module.exports = {
  initialize,
  get,
  set,
};

