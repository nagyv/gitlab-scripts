const fs = require("fs");
const path = require("path");
const webpack = require("webpack");

module.exports = {
  mode: "production",
  entry: "./src/index.js",
  target: "web",
  output: {
    filename: "gitlab.user.js",
    path: __dirname,
    publicPath: "",
  },
  optimization: {
    minimize: false,
  },
  plugins: [
    new webpack.BannerPlugin({
      banner: () => {
        const version = JSON.parse(fs.readFileSync(path.join(__dirname, "package.json"), "utf8")).version;
        const meta = fs.readFileSync(path.join(__dirname, "src", "meta.txt"), "utf8");
        return meta.replace("<version>", version);
      },
      entryOnly: true,
      raw: true,
    }),
  ],
};
